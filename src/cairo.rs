extern crate cairo;

use {
    DemoXYRange, RGBA8, XYDiagram, XYPoint, XYRange, XYSeries,
    XYSeriesCollection, RGBA,
};

/// A target for plotting charts.
pub trait PlotTarget {
    /*
    /// Plot an XYSeries.
    fn plot<S: XYSeries>(&mut self, series: S);
    */

    /// Plot an XY diagram.
    fn plot<D: XYDiagram>(&mut self, diagram: &D, width: f64, height: f64);
}

trait PlotTargetPrivate {
    fn plot_series<S: XYSeries, RFrom: XYRange, RTo: XYRange>(
        &mut self,
        series: &S,
        from_range: &RFrom,
        to_range: &RTo,
    );
    fn set_source_color(&self, color: &RGBA8);
}

impl PlotTargetPrivate for cairo::Context {
    fn plot_series<S: XYSeries, RFrom: XYRange, RTo: XYRange>(
        &mut self,
        series: &S,
        from_range: &RFrom,
        to_range: &RTo,
    ) {
        self.save();
        {
            self.set_line_cap(cairo::LineCap::Round);
            self.set_line_join(cairo::LineJoin::Round);
            self.set_source_color(&series.color());
            for (i, point) in series.points().iter().enumerate() {
                let (x, y) = point
                    .transform_from(from_range)
                    .transform_to(to_range);
                if i == 0 {
                    self.move_to(x, y);
                } else {
                    self.line_to(x, y);
                }
            }
            self.stroke();
        }
        self.restore();
    }

    fn set_source_color(&self, color: &RGBA8) {
        let RGBA { r, g, b, a } = color;
        self.set_source_rgba(
            *r as f64 / 255.0,
            *g as f64 / 255.0,
            *b as f64 / 255.0,
            *a as f64 / 255.0,
        );
    }
}

impl PlotTarget for cairo::Context {
    fn plot<D: XYDiagram>(
        &mut self,
        diagram: &D,
        width: f64,
        height: f64,
    ) {
        {
            self.save();
            self.rectangle(0f64, 0f64, width, height);
            self.set_source_rgb(1.0, 1.0, 1.0);
            self.fill();
            self.restore();
        }

        let m_hor = diagram.margin_horizontal();
        let m_ver = diagram.margin_vertical();

        let m_top = m_ver + if let Some(title) = diagram.title() {
            self.save();

            self.set_font_size(20f64);
            self.select_font_face(
                "sans-serif",
                cairo::FontSlant::Normal,
                cairo::FontWeight::Bold,
            );

            let extents = self.text_extents(&title);

            self.set_source_rgb(0.4, 0.4, 0.4);
            self.move_to(
                (width - extents.width) * 0.5,
                m_ver * 0.5 - extents.y_bearing,
            );
            self.show_text(&title);
            self.restore();
            extents.height
        } else {
            0f64
        };

        {
            self.save();
            let dw = width - m_hor - m_hor;
            let dh = height - m_ver - m_top;
            self.rectangle(m_hor, m_top, dw, dh);
            self.set_source_rgb(0.95, 0.95, 0.95);
            self.fill_preserve();

            self.set_source_rgb(0.7, 0.7, 0.7);
            self.stroke_preserve();

            let view_range = DemoXYRange {
                x_min: 0f64,
                x_max: dw,
                y_min: 0f64,
                y_max: dh,
            };

            {
                self.save();
                self.clip();
                self.translate(m_hor, m_top);
                self.scale(1.0, -1.0);
                self.translate(0f64, -dh);

                {
                    // x grid lines
                    self.save();
                    self.set_source_rgb(0.8, 0.8, 0.8);
                    let grid_x = diagram.grid_lines_x() + 1;
                    for grid in 0usize..grid_x {
                        let grid_x = grid as f64 / grid_x as f64;
                        let (x, y) =
                            (grid_x, 0f64).transform_to(&view_range);
                        self.move_to(x, y);
                        let (x, y) =
                            (grid_x, 1f64).transform_to(&view_range);
                        self.line_to(x, y);
                        self.stroke();
                    }
                    self.restore();
                }

                {
                    // y grid lines
                    self.save();
                    self.set_source_rgb(0.8, 0.8, 0.8);
                    let grid_y = diagram.grid_lines_y() + 1;
                    for grid in 0usize..grid_y {
                        let grid_y = grid as f64 / grid_y as f64;
                        let (x, y) =
                            (0f64, grid_y).transform_to(&view_range);
                        self.move_to(x, y);
                        let (x, y) =
                            (1f64, grid_y).transform_to(&view_range);
                        self.line_to(x, y);
                        self.stroke();
                    }
                    self.restore();
                }

                for curve in diagram.curves().series().iter() {
                    self.plot_series(curve, &diagram.range(), &view_range);
                }
                self.restore();
            }

            self.restore();
        }
    }
}
