#![deny(missing_docs)]

//! A library for plotting charts.
//!
//! # Warning
//! This is currently not much more than an experiment,
//! please expect things to not work or to break!

extern crate rgb;

/// Plotting functionalty for the cairo library.
#[cfg(feature = "cairo")]
pub mod cairo;

// TODO: make this work with any RGB type defined in the rgb crate
use rgb::{RGBA8, RGBA};

/// A point on an XY coordinate system.
pub trait XYPoint {
    /// Get the coordinates of the point.
    fn coordinates(&self) -> (f64, f64);

    /// Transform point from a range.
    fn transform_from<R: XYRange>(&self, range: &R) -> (f64, f64) {
        let (x, y) = self.coordinates();

        let x_min = range.x_min();
        let w = range.x_max() - x_min;

        let y_min = range.y_min();
        let h = range.y_max() - y_min;

        let x = (x - x_min) / w;
        let y = (y - y_min) / h;

        (x, y)
    }

    /// Transform a from a range.
    fn transform_to<R: XYRange>(&self, range: &R) -> (f64, f64) {
        let (x, y) = self.coordinates();

        let x_min = range.x_min();
        let w = range.x_max() - x_min;

        let y_min = range.y_min();
        let h = range.y_max() - y_min;

        let x = x * w + x_min;
        let y = y * h + y_min;

        (x, y)
    }
}

/// A series of points on an XY coordinate system.
pub trait XYSeries {
    /// The type of point used in the series.
    type Point: XYPoint;

    /// Get all the points.
    ///
    /// This is a preliminary solution due to missing knowledge about
    /// how to expose iterators completely transparent using generics.
    /// The performance is expected to be really bad for this due to
    /// copies which are actually not necessary when done right.
    fn points(&self) -> &Vec<Self::Point>;

    /// Get the color of the series.
    fn color(&self) -> RGBA8;
}

/// A collection of XY series.
pub trait XYSeriesCollection {
    /// The type of the series.
    type Series: XYSeries;

    /// Get all the curves.
    ///
    /// This is a preliminary solution due to missing knowledge about
    /// how to expose iterators completely transparent using generics.
    /// The performance is expected to be really bad for this due to
    /// copies which are actually not necessary when done right.
    fn series(&self) -> &Vec<Self::Series>;
}

impl<T: XYSeries> XYSeriesCollection for Vec<T> {
    type Series = T;

    fn series(&self) -> &Vec<Self::Series> {
        self
    }
}

/// A concrete implementation of a range in an XY coordinate system.
pub struct DemoXYRange {
    x_min: f64,
    x_max: f64,
    y_min: f64,
    y_max: f64,
}

impl XYRange for DemoXYRange {
    fn x_min(&self) -> f64 {
        self.x_min
    }
    fn x_max(&self) -> f64 {
        self.x_max
    }
    fn y_min(&self) -> f64 {
        self.y_min
    }
    fn y_max(&self) -> f64 {
        self.y_max
    }
}

/// A range in an XY coordinate system.
pub trait XYRange {
    /// The x minimum point.
    fn x_min(&self) -> f64;
    /// The x maximum point.
    fn x_max(&self) -> f64;
    /// The y minimum point.
    fn y_min(&self) -> f64;
    /// The y maximum point.
    fn y_max(&self) -> f64;
}

impl XYPoint for (f64, f64) {
    fn coordinates(&self) -> (f64, f64) {
        *self
    }
}

/// A XY diagram.
pub trait XYDiagram {
    /// The type of series used in the diagram.
    type Curves: XYSeriesCollection;

    /// The type of the XY view range.
    type Range: XYRange;

    /// The left and right of the diagram.
    fn margin_horizontal(&self) -> f64;

    /// The above and below the diagram.
    fn margin_vertical(&self) -> f64;

    /// Get the curve (preliminary, will be a set of curves in the future).
    fn curves(&self) -> Self::Curves;

    /// Get the range that should be shown.
    fn range(&self) -> Self::Range;

    /// Get the title.
    fn title(&self) -> Option<String>;

    /// The number of grid lines in the x direction.
    ///
    /// 0 means that no grid lines are shown in this direction.
    fn grid_lines_x(&self) -> usize;

    /// The number of grid lines in the y direction.
    ///
    /// 0 means that no grid lines are shown in this direction.
    fn grid_lines_y(&self) -> usize;
}

impl XYDiagram for DemoXYDiagram {
    type Curves = Vec<DemoXYSeries>;
    type Range = DemoXYRange;

    fn margin_horizontal(&self) -> f64 {
        self.margin_horizontal
    }
    fn margin_vertical(&self) -> f64 {
        self.margin_vertical
    }

    fn curves(&self) -> Self::Curves {
        vec![
            DemoXYSeries {
                color: RGBA {
                    r: 255,
                    g: 0,
                    b: 0,
                    a: 255,
                },
                points: vec![
                    (0f64, 0f64),
                    (10f64, 10f64),
                    (50f64, 40f64),
                    (70f64, 30f64),
                    (300f64, 30f64),
                    (350f64, 300f64),
                    (400f64, 300f64),
                    (410f64, 200f64),
                ],
            },
            DemoXYSeries {
                color: RGBA {
                    r: 0,
                    g: 0,
                    b: 255,
                    a: 255,
                },
                points: vec![
                    (0f64, 30f64),
                    (10f64, 150f64),
                    (50f64, 20f64),
                    (70f64, 22f64),
                    (300f64, 300f64),
                    (350f64, 30f64),
                    (400f64, 30f64),
                    (410f64, 20f64),
                ],
            },
        ]
    }

    fn range(&self) -> Self::Range {
        DemoXYRange {
            x_min: 0f64,
            x_max: 800f64,
            y_min: 0f64,
            y_max: 300f64,
        }
    }

    fn title(&self) -> Option<String> {
        Some("Demo Diagram".into())
    }

    fn grid_lines_x(&self) -> usize {
        3usize
    }

    fn grid_lines_y(&self) -> usize {
        10usize
    }
}

/// An XY diagram.
pub struct DemoXYDiagram {
    margin_horizontal: f64,
    margin_vertical: f64,
}

impl Default for DemoXYDiagram {
    fn default() -> Self {
        Self {
            margin_horizontal: 20f64,
            margin_vertical: 20f64,
        }
    }
}

/// An XY curve.
pub struct DemoXYSeries {
    color: RGBA8,
    points: Vec<(f64, f64)>,
}

impl XYSeries for DemoXYSeries {
    type Point = (f64, f64);

    fn points(&self) -> &Vec<Self::Point> {
        &self.points
    }

    fn color(&self) -> RGBA8 {
        self.color
    }
}
