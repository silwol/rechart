extern crate cairo;
extern crate rechart;

use cairo::{Context, Format, ImageSurface};
use rechart::cairo::PlotTarget;
use rechart::DemoXYDiagram;
use std::fs::File;

fn main() {
    let width = 1200;
    let height = 800;
    let surface =
        ImageSurface::create(Format::ARgb32, width, height).unwrap();

    let diagram = DemoXYDiagram::default();

    let mut context = Context::new(&surface);
    context.plot(&diagram, width as f64, height as f64);

    let mut file = File::create("target/output.png").unwrap();

    surface.write_to_png(&mut file).unwrap();
}
